class Notas{            
    
    constructor(id, titulo, contenido, modificacion, categoria){
        this.id = id;
        this.titulo = titulo;
        this.contenido = contenido;
        this.modificacion = modificacion;
        this.categoria = categoria;
    }

    set setId(id){
        this.id = id;
    }

    get getId(){
        return this.id;
    }

    set setTitulo(titulo){
        this.titulo = titulo;
    }

    get getTitulo(){
        return this.titulo;
    }

    set setContenido(contenido){
        this.contenido = contenido;
    }

    get getContenido(){
        return this.contenido;
    }

    set setModificacion(modificacion){
        this.modificacion = modificacion;
    }

    get getModificacion(){
        return this.modificacion;
    }

    set setCategoria(categoria){
        this.categoria = categoria;
    }

    get getCategoria(){
        return this.categoria;
    }
}
class Categoria{
    constructor(categoria, modificacion){
        this.categoria = categoria;
        this.modificacion = modificacion;
    }
    
    set setCategoria(categoria){
        this.categoria = categoria;
    }

    get getCategoria(){
        return this.categoria;
    }
    set setModificacion(modificacion){
        this.modificacion = modificacion;
    }

    get getModificacion(){
        return this.modificacion;
    }
}


function altanotas() {
    let titulo = document.getElementById('titulo');
    let contenido = document.getElementById('contenido');
    let select = document.getElementsByName("categoria")[0];
    let categoria = select.value;
    //alert(document.getElementsByName("categoria")[0].value);

    //Verifico que estén todos los campos
    if(titulo.value != "" && contenido.value != "" && titulo.value.length < 61 && contenido.value.length < 501 && select.value != ""){
        //creo el objeto
        let i = 20;
        let id = generarId(i);
        let modificacion = generarModi();

        let nota1 = new Notas(
            id,
            titulo.value,
            contenido.value,
            modificacion,
            categoria
        )

        //verifico que no exista valido por titulo
        let buscar = JSON.parse(localStorage.getItem('Notas'));
        if(!buscar){
            buscar = [];
        }
        console.log(buscar);
        //console.log(nota1.titulo);
        console.log(buscar.find( notabus => notabus.titulo === nota1.titulo));
        if (buscar.find( notabus => notabus.titulo === nota1.titulo))
            alert("Ya existe una Nota con este Titulo!!!...")
        else{

            let nota2 = JSON.parse(localStorage.getItem('Notas'));
            
            //Controlo si no tengo aún Notas almacenadas
            if(!nota2){
                nota2 = [];
            }
            nota2.push(nota1);
            localStorage.setItem('Notas',JSON.stringify(nota2));
            
            //Limpio el formulario
            titulo.value = ""
            contenido.value = ""
            alert('La Nota se guardo correctamente!!!...');
        }
    } else {
        if (titulo.value == "" || contenido.value == "" ){ 
            alert('Campos incompletos!!!...');
        }
        if (titulo.value.length > 61 || contenido.value.length > 501){ 
            alert('Supera la cantidad Caracteres!!!...');
        }
        if (select.value != ""){ 
            alert('No puede Agregar una nota Sin Categoria!!!...');
        }
    }    
}

function mostrarNotas() {
    console.log("Si Lo ejecuto");
    let filtro = document.getElementById('buscar');
    console.log(filtro.value);
    let notas2 = JSON.parse(localStorage.getItem('Notas'));
    let notas3 = null;
    //Inicializo la lista con todos
    let select = document.getElementsByName("catefiltro")[0];
    select.value = "Todos";
    console.log(notas2);
    if (filtro.value != ""){   
        console.log("no esta vacio buscar");

         /**
         * Filtra la matríz en función de un criterio de búsqueda (query)
         * nota es cada uno de los obj del arreglo que obj y lo devuelve si coincide 
         */
        let notas4 = notas2.filter((nota) => 
         { console.log("nota:" + nota.titulo);
            //return nota.titulo.toLowerCase().indexOf(filtro.value.toLowerCase()) > -1 });
            //alert("por titulo "+nota.titulo.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //alert("por modif "+nota.contenido.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //filter devuelve -1 si no encuentra coincidencia, sumo las 2 buquedas y si hay una coincide nunca sera -2
            return ((nota.titulo.toLowerCase().indexOf(filtro.value.toLowerCase())) + (nota.contenido.toLowerCase().indexOf(filtro.value.toLowerCase())) > -2)});
        console.log("notas2");
        console.log(notas2 = notas4);
    }
    //Controlo si no tengo aún notas almacenados
    if(!notas2){
        notas2 = [];
    }
    //Genero el contenido de la tabla

    let tabla = "";
    for (let index = 0; index < notas2.length; index++) {
        let notas1 = notas2[index];
        //borrar y modificar enviando id 
        // tabla += '<tr><td>'+notas1.id+'</td><td>'+notas1.titulo+'</td><td>'+notas1.contenido+'</td><td>'+notas1.categoria+'</td><td>'+notas1.modificacion+'</td><td>'+'<button type="button" class="btn btn-danger btn-sm" onclick="borrarId(\''+notas1.id+'\')"><i class="fas fa-minus-circle"></i> Borrar</button></td>';
        tabla += '<tr><td>'+notas1.titulo+'</td><td>'+notas1.contenido+'</td><td>'+notas1.categoria+'</td><td>'+notas1.modificacion+'</td><td>'+'<button type="button" class="btn btn-danger btn-sm" onclick="borrarId(\''+notas1.id+'\')"><i class="fas fa-minus-circle"></i> Borrar</button></td>';
        tabla += '<td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cargarmodalModiNota(\''+notas1.id+'\')" data-target="#exampleModal"><i class="fas fa-exchange-alt"></i> Modificar</button></td></tr>';
    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaNotas').innerHTML = tabla;
}

function generarId(length) {
    var id = '';
    var caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var caracteresLength = caracteres.length;
    for (var i = 0; i < length; i++) {
        id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
    }
    console.log(id);
    return id;
}

function generarModi(){
    let mostrar = "";
    let fecha = new Date();
    let i = 0;
    console.log(mostrar);
    console.log(fecha);
    if (fecha.getDate()<10)
    {
    mostrar = mostrar + "0" + fecha.getDate().toString() + "/";
    } else {
    mostrar = mostrar + fecha.getDate().toString() + "/"; 
    }
    console.log(mostrar);
    if(fecha.getMonth()<9)
    {
    i = fecha.getMonth() + 1;
    mostrar = mostrar + "0" + i.toString() + "/";
    } else{
    mostrar = mostrar + "0" + fecha.getMonth().toString() + "/";        
    }
    console.log(mostrar);
    mostrar = mostrar + fecha.getFullYear().toString();
    console.log(mostrar);
    mostrar = mostrar + " " + fecha.getHours().toString() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    console.log(mostrar);
    return mostrar;
}

function borrarId(mid = null){
    let notas2 = JSON.parse(localStorage.getItem('Notas'));
    if ((mid != null) && (notas2 != null))
    {
        console.log("si entro a buscar");

        let indice = notas2.findIndex(notasbus => notasbus.id === mid);
        console.log(indice);
        console.log(notas2[indice]);

        if(indice > -1)
        {
            if(confirm("¿Borra?")){
                notas2.splice(indice,1);
                localStorage.setItem('Notas',JSON.stringify(notas2));
                mostrarNotas();
            }           
        }
    }    
}

function cargarmodalModiNota(mid = null){
    console.log(mid);
    let notas2 = JSON.parse(localStorage.getItem('Notas'));
    if ((mid != null) && (notas2 != null))
    {
        console.log("si entro a buscar");

        let indice = notas2.findIndex(notabus => notabus.id === mid);
        console.log(indice);
        console.log(notas2[indice]);

        document.getElementById('idmodal').value = notas2[indice].id;
        document.getElementById('titulomodal').value = notas2[indice].titulo;
        document.getElementById('contenidomodal').value = notas2[indice].contenido;
        document.getElementById('modificacionmodal').value = notas2[indice].modificacion;
        document.getElementById("inputGroupSelect01").value = notas2[indice].categoria;
    }    
}

function Modinota(){
    let id = document.getElementById('idmodal');
    let contenido = document.getElementById('contenidomodal');
    let select = document.getElementsByName("categoria")[0];
    let categoria = select.value;
    let nota2 = JSON.parse(localStorage.getItem('Notas'));

    let indice = nota2.findIndex(notabus => notabus.id === id.value);
    console.log(indice);
    console.log(nota2[indice]);

    if(indice > -1 && contenido.value != "" && contenido.value.length < 501 )
    {
        if(confirm("¿Confirma Modificacion de la Nota?")){
            let modificacion = generarModi();
            nota2[indice].contenido = contenido.value;
            nota2[indice].modificacion = modificacion;
            nota2[indice].categoria = categoria;
            localStorage.setItem('Notas',JSON.stringify(nota2));
            mostrarNotas();
            $('#exampleModal').modal('hide')
        }           
    } else {
        alert('Supera la cantidad Maxima de Caracteres!!!...');
    }    

}

function altacategoria() {
    let categoria = document.getElementById('categoria');

    //Verifico los campos
    if(categoria.value != "" && categoria.value.length < 31){
        //creo el objeto
        let modificacion = generarModi();

        let categoria1 = new Categoria(
            categoria.value,
            modificacion
        )

        //verifico que no exista valido por categoria
        let buscar = JSON.parse(localStorage.getItem('Categoria'));
        if(!buscar){
            buscar = [];
        }
        console.log(buscar);
        //console.log(categoria1.categoria);
        console.log(buscar.find( categoriabus => categoriabus.categoria === categoria1.categoria));
        if (buscar.find( categoriabus => categoriabus.categoria === categoria1.categoria))
            alert("Ya existe una Categoria con este Nombre!!!...")
        else{

            let categoria2 = JSON.parse(localStorage.getItem('Categoria'));
            
            //Controlo si no tengo aún Categoria almacenadas
            if(!categoria2){
                categoria2 = [];
            }
            categoria2.push(categoria1);
            localStorage.setItem('Categoria',JSON.stringify(categoria2));
            
            //Limpio el formulario
            categoria.value = ""
            alert('La Categoria se guardo correctamente!!!...');
        }
    } else {
        alert('Campos incompleto o Supera la cantidad Maxima de Caracteres!!!...');
    }    
}

function mostrarCategorias() {
    console.log("Si Lo ejecuto");
    let filtro = document.getElementById('buscar');
    console.log(filtro.value);
    let categoria2 = JSON.parse(localStorage.getItem('Categoria'));

    console.log(categoria2);
    if (filtro.value != ""){   
        console.log("no esta vacio buscar");

         /**
         * Filtra la matríz en función de un criterio de búsqueda (query)
         * categoria es cada uno de los obj del arreglo que obj y lo devuelve si coincide 
         */
        let categoria4 = categoria2.filter((categoria) => 
         { console.log("categoria:" + categoria.titulo);
            //return categoria.titulo.toLowerCase().indexOf(filtro.value.toLowerCase()) > -1 });
            //alert("por titulo "+categoria.titulo.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //alert("por modif "+categoria.contenido.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //filter devuelve -1 si no encuentra coincidencia, sumo las 2 buquedas y si hay una coincide nunca sera -2
            return categoria.categoria.toLowerCase().indexOf(filtro.value.toLowerCase()) > -1 });
        console.log("categoria2");
        console.log(categoria2 = categoria4);
    }
    //Controlo si no tengo aún categorias almacenados
    if(!categoria2){
        categoria2 = [];
    }
    //Genero el contenido de la tabla

    let tabla = "";
    for (let index = 0; index < categoria2.length; index++) {
        let categoria1 = categoria2[index];
        //borrar y modificar enviando nombre categoria 
        tabla += '<tr><td>'+categoria1.categoria+'</td><td>'+categoria1.modificacion+'</td><td>'+'<button type="button" class="btn btn-danger btn-sm" onclick="borrarCategoria(\''+categoria1.categoria+'\')"><i class="fas fa-minus-circle"></i> Borrar</button></td>';
        // tabla += '<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="cargarmodalModiCate(\''+categoria1.categoria+'\')" data-target="#exampleModal">Modificar</button></td></tr>';
    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaCategoria').innerHTML = tabla;
}

function borrarCategoria(mcate = null){
    let nota2 = JSON.parse(localStorage.getItem('Notas'));

    let indicenota = nota2.findIndex(notabuscate => notabuscate.categoria === mcate);

    if (indicenota < 0){ 
        let categoria2 = JSON.parse(localStorage.getItem('Categoria'));
        if ((mcate != null) && (categoria2 != null))
        {
            console.log("si entro a buscar");

            let indice = categoria2.findIndex(categoriabus => categoriabus.categoria === mcate);
            console.log(indice);
            console.log(categoria2[indice]);
            //alert(indice);

            if(indice > -1)
            {
                if(confirm("¿Borra?")){
                    categoria2.splice(indice,1);
                    localStorage.setItem('Categoria',JSON.stringify(categoria2));
                    mostrarCategorias();
                }           
            }
        }
    } else {
        alert('No puede Borrar esta Categoria, Debido a que hay Notas Usandola!!!...');
    }
}

/* function cargarmodalModiCate(mcate = null){
    console.log(mcate);
    let categoria2 = JSON.parse(localStorage.getItem('Categoria'));
    if ((mcate != null) && (categoria2 != null))
    {
        console.log("si entro a buscar");

        let indice = categoria2.findIndex(categoriabus => categoriabus.categoria === mcate);
        console.log(indice);
        console.log(categoria2[indice]);

        document.getElementById('categoriamodal').value = categoria2[indice].categoria;
        document.getElementById('modificacionmodal').value = categoria2[indice].modificacion;
    }    
} */
// Rutina para agregar opciones a un <select>
function addOptions(domElement) {
    var select = document.getElementsByName(domElement)[0];
    select.innerHTML = "";
    //borro todos los items
    //document.getElementById("inputGroupSelect01").options.length = 0;
/*     document.getElementById("inputGroupSelect01").options.length = 0;
    document.getElementById("catefiltro").options.length = 0; */
    //document.getElementsByName(domElement).options.length = 0;
    var option= null;
    if (domElement == "catefiltro")
    {
        option = document.createElement("option");
        option.text = "Todos"
        select.add(option);
    }

    let categoria2 = JSON.parse(localStorage.getItem('Categoria'));
    //Controlo si no tengo aún categorias almacenados
    if(!categoria2){
        categoria2 = [];
    }

    for (let index = 0; index < categoria2.length; index++) {
        let categoria1 = categoria2[index];

        //for (value in array) {
        option = document.createElement("option");
        option.text = categoria1.categoria;
        select.add(option);
    }
}
function FiltrarCate(){
    let select = document.getElementsByName("catefiltro")[0];
    let filtro = select;

    document.getElementById('buscar').value = "";
    let notas2 = JSON.parse(localStorage.getItem('Notas'));
    let notas3 = null;

    if (filtro.value != "Todos"){   
        console.log("no esta vacio buscar");

         /**
         * Filtra la matríz en función de un criterio de búsqueda (query)
         * nota es cada uno de los obj del arreglo que obj y lo devuelve si coincide 
         */
        let notas4 = notas2.filter((nota) => 
         { console.log("nota:" + nota.categoria);
            //return nota.categoria.toLowerCase().indexOf(filtro.value.toLowerCase()) > -1 });
            //alert("por categoria "+nota.categoria.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //alert("por modif "+nota.contenido.toLowerCase().indexOf(filtro.value.toLowerCase()));
            //filter devuelve -1 si no encuentra coincidencia, sumo las 2 buquedas y si hay una coincide nunca sera -2
            return nota.categoria.toLowerCase().indexOf(filtro.value.toLowerCase()) > -1 });
        console.log("notas2");
        console.log(notas2 = notas4);
    }
    //Controlo si no tengo aún notas almacenados
    if(!notas2){
        notas2 = [];
    }
    //Genero el contenido de la tabla

    let tabla = "";
    for (let index = 0; index < notas2.length; index++) {
        let notas1 = notas2[index];
        //borrar y modificar enviando id 
        // tabla += '<tr><td>'+notas1.id+'</td><td>'+notas1.titulo+'</td><td>'+notas1.contenido+'</td><td>'+notas1.categoria+'</td><td>'+notas1.modificacion+'</td><td>'+'<button type="button" class="btn btn-danger btn-sm" onclick="borrarId(\''+notas1.id+'\')"><i class="fas fa-minus-circle"></i> Borrar</button></td>';
        tabla += '<tr><td>'+notas1.titulo+'</td><td>'+notas1.contenido+'</td><td>'+notas1.categoria+'</td><td>'+notas1.modificacion+'</td><td>'+'<button type="button" class="btn btn-danger btn-sm" onclick="borrarId(\''+notas1.id+'\')"><i class="fas fa-minus-circle"></i> Borrar</button></td>';
        tabla += '<td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" onclick="cargarmodalModiNota(\''+notas1.id+'\')" data-target="#exampleModal"><i class="fas fa-exchange-alt"></i> Modificar</button></td></tr>';
    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaNotas').innerHTML = tabla;


}